from django.urls import path, include
from django.contrib.auth import views as auth_views
from .views import *


urlpatterns = [
    # LogIn & LogOut & Register & Edit
    path('login/', auth_views.LoginView.as_view(), name='login'),
    path('logout/', auth_views.LogoutView.as_view(), name='logout'),
    path('register/', register, name='register'),
    path('edit/', edit, name='edit'),
    # Dashboard
    path('', dashboard, name='dashboard'),
]